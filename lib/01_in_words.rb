class Fixnum


    ONES = {
      0 => "zero",
      1 => "one",
      2 => "two",
      3 => "three",
      4 => "four",
      5 => "five",
      6 => "six",
      7 => "seven",
      8 => "eight",
      9 => "nine"
    }

    TENS = {
      10 => "ten",
      20 => "twenty",
      30 => "thirty",
      40 => "forty",
      50 => "fifty",
      60 => "sixty",
      70 => "seventy",
      80 => "eighty",
      90 => "ninety"
    }

    TEENS = {
      10 => "ten",
      11 => "eleven",
      12 => "twelve",
      13 => "thirteen",
      14 => "fourteen",
      15 => "fifteen",
      16 => "sixteen",
      17 => "seventeen",
      18 => "eighteen",
      19 => "nineteen"
    }

    MAGNITUDES = {
      100 => "hundred",
      1000 => "thousand",
      1_000_000 => "million",
      1_000_000_000 => "billion",
      1_000_000_000_000 => "trillion"

    }

# 1. write out possible scenarios for self as conditional statements
# 2. what will occur in each case?
  def in_words
    if self < 10
      ONES[self]
    elsif self < 20
      TEENS[self]
    elsif self < 100
      tens_word = TENS[(self/10)*10]
      if self % 10 != 0
         tens_word + " " + (self % 10).in_words
      else
        tens_word
      end
    else
      magnitude = MAGNITUDES.keys.select{|number| number <= self}.last
      result = (self/magnitude).in_words + " " + MAGNITUDES[magnitude]
      if (self%magnitude) != 0
        result += " " + (self % magnitude).in_words
      end
      result
    end
  # result
  end
end


  #   ONES_place = [nil, "one", "two", "three", "four", "five",
  #      "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
  #         "sixteen", "seventeen", "eighteen", "nineteen"]
  #   tens_place = [nil, "ten", "twenty", "thirty", "forty", "fifty",
  #      "sixty", "seventy", "eighty", "ninety"]
  #   # teens = [nil, ]
  #   large_nums = [nil, "hundred", "thousand", "million", "billion",
  #      "trillion"]
  # # end
  # def put_into_words(number)
#     number_string = ''
#
#     if self < 0
#       raise 'No negative numbers'
#     end
#
#     return 'zero'if self == 0
#
#     if self < 20
#       number_string << ones_place[self]
#     elsif self >= 20 && self < 100 && self % 10 == 0
#       number_string << tens_place[self/10]
#     elsif self >= 20 && self < 100
#       number_string << tens_place[self/10] + " " + ones_place[self%10]
#     elsif self >= 100 && self < 1000 && self % 100 == 0
#       number_string << ones_place[self/100] + " " + large_nums[(self/100).to_s.length.to_i]
#     elsif self >= 100 && self < 1000
#       number_string << ones_place[self/100] + " " + large_nums[(self/100).to_s.length.to_i] + " " + tens_place[self.to_s.chars[-2..-1].join.to_i/10] + " " + ones_place[self%10]
#
#     else
#       puts "not there yet."
#     end
#
#     number_string
#   end
#
# end


#     left = number
#     right = left/1_000_000_000_000
#     left = left - right*1_000_000_000_000
#
#     if right < 0
#       trillions = ones[right]
#       num_string = num_string + trillions + 'trillion'
#
#     if left > 0
#       num_string = num_string + ' '
#     end
#     end

#   right = left/1_000_000_000_00
#   left = left - right*1_000_000_000_00
#

#   if right > 0
#     if ((right == 1) and (left > 0))
#       num_string = num_string + teenagers[left-1]
#       left = 0
#     else
#       num_string = num_string + tens_place[right-1]
#     end
#
#     if left > 0
#       num_string = num_string + '-'
#     end
#   end
#
#   right = left
#   left = 0
#
#   if right > 0
#     num_string = num_string + ones_place[right-1]
#   end
#
#   num_string
#   end
#
# end
